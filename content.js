var content = [
	{
		"cssId": "et",
		"title": "et",
		"content":
		"<p>" +  
		"<span><b>NA</b> 17 1-69</span><br />" +
		"<span><b>NA</b> 13 70-89</span><br />" +
		"<span><b>NA</b> 11 90</span><br />" +
		"<span><b>NA</b> 12 91-99</span><br />" +
		"<span><b>NA</b> 13 100</span><br /><br />" +
		"<span><b>by Hinoka</b></span><br />" +
		"</p>"
	},
	
	{
		"cssId": "valhalla",
		"title": "valhalla",
		"content": "<p><img src='/assets/vr/vrall' /></p>" 
	},
	
	{
		"cssId": "war",
		"title": "war",
		"content": "war"
	},

	{
		"cssId": "guides",
		"title": "guides",
		"content": "guides"
	},

	{
		"cssId": "rules",
		"title": "rules",
		"content":
		"<div>" +
		"<b>Punições</b>: <br />" +
		"<span>1- BAN (kick definitivo)</span><br />" +
		"<span>> Recorrencia de qualquer outro motivo definido abaixo validados pela liderença</span><br />" +
		"<span>> Preconceito (xenofobia, homofobia, etc...)</span><br />" +
		"<span>> Uso de Addons (pela liderença)</span><br />" +
		"<br />" +
		"<span>2- Kick (Temp)</span><br />" +
		"<span>> Recorrencia de qualquer outro motivo definido abaixo validados pela liderença</span><br />" +
		"<span>> Ação indevida na WOE e WOC</span><br />" +
		"<br />" +
		"<span>3- Block nas RUN ET,TT,VAL,ORACLE,DOJO</span><br />" +
		"<span>> 3 semanas sem contribuição na guilda</span><br />" +
		"<br />" +
		"<span>4- Low Priority nas RUN ET,TT,VAL,ORACLE,DOJO</span><br />" +
		"<span>> 2 semanas sem contribuição na guilda</span><br />" +
		"<span>> AFK nas RUNs ou não seguindo regra da pessoa carregando</span><br />" +
		"<br />" +
		"<span>5- Doação na guilda como compensação</span><br />" +
		"<span>> 1 semanas sem contribuição na guilda</span><br />" +
		"<br />" +
		'<span>6- Poisition "Jail" plus chamada atenção</span><br />' +
		"</div>"
	}
]
