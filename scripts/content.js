"use strict"

var entityMap = {
	'&': '&amp;',
	'<': '&lt;',
	'>': '&gt;',
	'"': '&quot;',
	"'": '&#39;',
	'/': '&#x2F;',
	'`': '&#x60;',
	'=': '&#x3D;'
};

function escapeHtml(string) {
	return String(string).replace(/[&<>"'`=\/]/g, function (s) {
		return entityMap[s];
	});
};

var blockElementFactory = function (cssId, title, content) {
	return (
		'<div id="' + cssId + '" class="card">' +
			'<div class="title">' +
				'<span>' + title + '</span>' +
			'</div>' +

			'<div class="content">' + content + '</div>' +
		'</div>'
	);
};

function getContainerLevelElement() {
	return document.querySelector("#container");
};

function constructContent() {
	var contentBlocks = [];

	content.forEach(function (elem) {
		contentBlocks.push(blockElementFactory(elem.cssId, elem.title, elem.content));
	});

	var htmlObject = document.createElement('div');
	htmlObject.innerHTML = contentBlocks.join("");

	return htmlObject;
};

function bindSelectionToImages() {
	var imgs = document.querySelectorAll('.card img');
	var lightbox = document.querySelector('.lightbox')

	imgs.forEach(function(elem) {
		elem.addEventListener('click', function() {
			this.classList.toggle('img-active');
			lightbox.classList.toggle('display-none')
		})
	})
}

(function () {
	var container = getContainerLevelElement();
	var builtContent = constructContent();
	container.appendChild(builtContent);

	bindSelectionToImages();
})();
